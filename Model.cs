namespace DI
{
    public class Model : ModelBase, IModel
    {
        public Model(string firstName, string lastName, int age, bool gender) : base(age)
        {
            FirstName = firstName;
            LastName = LastName;
            Age = age;
            Gender = gender;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public bool Gender { get; set; }

        public string GetFullName() => $"{FirstName} {LastName}";
    }
}