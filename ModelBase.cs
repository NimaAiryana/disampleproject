namespace DI
{
    public abstract class ModelBase
    {
        private readonly long age;

        public ModelBase(long age) => this.age = age;

        public long ConvertAgeToSeconds() => age * 365 * 12 * 60 * 60;
    }
}