namespace DI
{
    public class CustomModel : ModelBase, IModel
    {
        public CustomModel(string firstName, string lastName, int age, bool gender) : base(age)
        {
            FirstName = firstName;
            LastName = LastName;
            Age = age;
            Gender = gender;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public bool Gender { get; set; }

        public string GetFullName()
        {
            string genderName = Gender ? "Mr" : "Mrs";

            return $"{genderName} {FirstName} {LastName}";
        }
    }
}