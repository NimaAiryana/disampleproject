namespace DI.Tests
{
    public class SampleControllerTests
    {
        public bool GetFullNameActionTest_WhenInjectModelClass_ShouldReturnFirstAndLastName()
        {
            var model = new Model("nima", "airyana", 27, true);

            var sampleController = new SampleController(model);

            if (sampleController.GetFullNameAction() == $"{model.FirstName} {model.LastName}") return true;

            return false;
        }

        public bool GetFullNameActionTest_WhenInjectCustomModelClass_ShouldReturnGenderNameWithFirstAndLastName()
        {
            var model = new CustomModel("nima", "airyana", 27, true);

            var sampleController = new SampleController(model);

            if (sampleController.GetFullNameAction() == $"Mr {model.FirstName} {model.LastName}") return true;

            return false;
        }
    }
}