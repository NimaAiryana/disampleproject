namespace DI
{
    public class SampleController
    {
        private readonly IModel model;

        public SampleController(IModel model) => this.model = model;

        public string GetFullNameAction() => model.GetFullName();
    }
}