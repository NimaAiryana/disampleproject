namespace DI
{
    public interface IModel
    {
        string FirstName { get; set; }

        string LastName { get; set; }

        int Age { get; set; }

        bool Gender { get; set; }

        long ConvertAgeToSeconds();

        string GetFullName();
    }
}